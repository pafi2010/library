# REQUIRE #

require php@7.0^

# READMI #

1. Клонируем репозиторий

2. Composer update

3. Настраиваем конфиг .env

4. php artisan migrate

5. php artisan db:seed

P.S. только 1я книга есть в магазине

# LINKS #

/ - main page

/books - all books

/books/:id - one book

/books/genre/:id - all the books of this genre
