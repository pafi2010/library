<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bulma.css">

    <style>
        .columns{
            justify-content: space-between;
        }
        .book{
            margin-left: -10px;
            margin-right: -10px;
        }
        .price{
            color: darkred;
            align-self: center;
        }
        small{
            color:grey;
        }
    </style>
</head>
<body>
<header class="section">
    <div class="container">
        <a href="{{url('/')}}"><h1 class="title is-1 has-text-centered">Книжный каталог</h1></a>
    </div>
</header>
<section class="section">
    <div class="container">
        <div class="columns">

            @include('sidebar')

            @yield('content')

        </div>
    </div>
</section>


</body>
</html>