@extends('layout')

@section('content')

    <div class="column">
        <div class="box columns is-multiline">

            @isset($books)
                @each('book', $books, 'book')
            @endisset

            @isset($book)
                @include('onebook')
            @endisset

        </div>

        @isset($pagination)
            <div>
                {{$pagination}}
            </div>
        @endisset

    </div>

@stop