    <div class="column is-one-quarter">
        <nav class="panel">
            <p class="panel-heading">
                Найти книгу по жанру
            </p>

        @foreach ($genres as $genre)
            <a class="panel-block" href="{{url('books/genre/'.$genre->id)}}">
                <span class="panel-icon">
                  <i class="fa fa-book"></i>
                </span>
                {{$genre->name}}
            </a>
        @endforeach

        </nav>
    </div>