<div class="book box column is-half">
    <div class="content columns">
        <div class="column">
            <a href="{{url('books/'.$book->id)}}"><strong>{{$book->name}}</strong></a>,&nbsp;{{$book->created_at->format('Y')}}
            <br>
            <small>//{{$book->genre->name}}</small>
        </div>
        <div class="price column has-text-right title is-5">{{$book->price}} руб.</div>

    </div>
    <p>
        {{$book->description}}
    </p>
</div>