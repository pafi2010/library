<div class="book box column">
    <div class="content columns">
        <div class="column">
            <strong>{{$book->name}}</strong>
            <br>
            <small>//{{$book->genre->name}}</small>
            <br>
            <small>Дата поступления: {{$book->created_at->format('Y-m-d')}}</small>
            <br>
            <small>Кол-во страниц: {{$book->page_amount}}</small>
            <br>
            <small>
                Наличие в магазинах:
                <ul>
                    @foreach($book->shops as $shop)
                        <li>{{$shop->name}}</li>
                    @endforeach
                </ul>
                @empty($book->shops->toArray())
                Нет в наличии
                @endempty
            </small>
        </div>
        <div class="price column has-text-right title is-5">{{$book->price}} руб.</div>

    </div>
    <p>
        {{$book->description}}
    </p>
</div>