<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3,true),
        'description' => $faker->sentence,
        'page_amount' => $faker->numberBetween(100, 500),
        'price' => $faker->randomFloat(2, 500, 1500),
        'genre_id' => $faker->randomDigitNotNull,
    ];
});

$factory->define(App\Genre::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\Shop::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
