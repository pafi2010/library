<?php

use Illuminate\Database\Seeder;
use App\Shop;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shop::create([
            'name' => 'Магазин А',
            'address' =>''
        ]);

        Shop::create([
            'name' => 'Магазин B',
            'address' =>''
        ]);

        Shop::create([
            'name' => 'Магазин C',
            'address' =>''
        ]);
    }
}
