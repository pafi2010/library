<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book_shop')->insert([
            'book_id' => 1,
            'shop_id' => 2
        ]);
        DB::table('book_shop')->insert([
            'book_id' => 1,
            'shop_id' => 3
        ]);

    }
}
