<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 300)->create();
        factory(App\Genre::class, 9)->create();
        $this->call(ShopTableSeeder::class);
        $this->call(BookShopTableSeeder::class);
    }
}
