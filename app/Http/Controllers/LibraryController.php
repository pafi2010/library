<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Shop;
use App\Genre;
use Illuminate\Support\Facades\View;

class LibraryController extends Controller
{
    public function index()
    {
        $data['books'] = Book::with('genre')->orderByDESC('created_at')->limit(5)->get();

        return $this->render($data);
    }

    public function getGenre($id)
    {
        $data['books'] = Book::where('genre_id',$id)->simplePaginate(15);
        $data['pagination'] = $data['books']->render();

        return $this->render($data);
    }

    public function getAllBooks()
    {
        $data['books'] = Book::simplePaginate(15);
        $data['pagination'] = $data['books']->render();

        return $this->render($data);
    }

    public function getOneBook($id)
    {
        $data['book'] = Book::find($id);

        return $this->render($data);
    }

    private function render($data)
    {
        $data['genres'] = Genre::all();
        return view('content', $data);
    }

}
