<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Genre;

class Book extends Model
{
    public function genre()
    {
        return $this->belongsTo(Genre::class,'genre_id');
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }
}
